# x52 Profiles #

## This is a new project ##

I have only just started this repository and cannot guarantee anything works yet. Use at your own risk

## Quick Start Guid##

Place the desired `*.pr0` file in `\Users\Public\Documents\SmartTechnology Profiles`. This will add the profile to your profile list for use. Any additional instructions can be found in the readme files of the inidividual profiles, if there are additional instructions.

### What is this repository for? ###

This repository is to hold x52 profiles for various software to save time configuring. Profiles ship with a default control layout, and all in-game controls will be defined in the profile for easy modification.

### How do I get set up? ###

Find the folder for the software you want to download a profile for, download the `.pr0` file and place it in `\Users\Public\Documents\SmartTechnology Profiles`. Be sure to follow any additional instructions in the readme file for that software. If there is no readme file, you're good to go. Load up the profile and go for it.

### Contribution guidelines ###

Please fork this repository and submit pull requests for new integrations.

### Who do I talk to? ###

Issues can be logged by clicking the issues icon on the left.