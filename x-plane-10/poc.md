x-plane surfaces 1191 commands

given a standard US keyboard, we have a total of 3 modifiers (in windows, CTL, ALT, SHIFT).

The following keys are available to use in shortcut commands:

| key | key | key |
|-|-|-|
| backspace | tab | enter
| pause/break | escape | page up
| page down | end | home
| left arrow | up arrow | right arrow
| down arrow | insert | delete
| 0 | 1 | 2
| 3 | 4 | 5
| 6 | 7 | 8
| 9 | a | b
| c | d | e
| f | g | h
| i | j | k
| l | m | n
| o | p | q
| r | s | t
| u | v | w
| x | y | z
| numpad 0 | numpad 1 | numpad 2
| numpad 3 | numpad 4 | numpad 5
| numpad 6 | numpad 7 | numpad 8
| numpad 9 | multiply | add
| subtract | decimal  | divide
| f1 | f2 | f3
| f4 | f5 | f6
| f7 | f8 | f9
| f10 | f11 | f12
| semi-colon | equal sign | comma
| dash | peri | forward slash
| grave accent | open bracket | back slash
| close bracket | single quote |


That gives me the following potential:
 - 89 single keys
 - 267 2-key combinations (`3 * 89`)
 - 267 3-key combinations (`(3 * 2) / 2 * 89`)
 - 89 4-key combinations (`1 * 89`)

 `89 + 267 + 267 + 89 = 712`

 That's several hundred less than the number of controls x-plane surfaces. This is based off my current understanding of how x-plane functions with key commands. There are some possibilities that would drastically increase the number of commands that can be programmed:

 - Does X-Plane recognize left vs right modifiers differently (L-Shift, R-Shift)? If so, I suddenly have 6 modifiers instead of 3.
   - 89 single
   - 534 2-key combinations (`6 * 89`)
   - 1335 3-key combinations (`(6 * 5) / 2 * 89`)
   - I don't need to go any further. That's plenty.
 - Can X-Plane use the windows key as another modifier key
  - 89 single
  - (`4 * 89`)
 - Can X-Plane use the apps key? Probably not that useful... maybe 1 more key though.
 - I can get a few more function keys using a virtual keyboard to get the rarely used F13-F24 from the old IBM 5250... maybe. That's a long shot, but could be possible.

If none of these options is possible, I'll be forced to leave some 500 commands off the stick. That might be okay anyway, as many can be eliminated using toggle controls instead of on/off controls. This does more than half the number of commands, since some things, like toggle fuel pumps, has independet controls for 8 fuel pumps (16 commands!) reduced to a single.
