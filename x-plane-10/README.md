The profile has used the default keyboard layout for all the keys that were defined, but many of the controls do not have a keybinding by default. Additional keyboard mappings are defined in the `X-Plane Keys.prf` file. Using this file should not break any existing bindings if you use the default controls. If not, it will overwrite your custom key bindings.

In order to use the new keyboard mappings, you need to place the `X-Plane Keys.prf` file in your X-Plane 10 install folder in the path `/Output/preferences`.

Make sure you installed the `.pr0` file, load up X-Plane 10, and fly.
